function rle(value) {
  let str = '';
  let counter = 1;
  for (let i = 1; i < value.length; i++) {
    if (value[i] === value[i - 1]) {
      counter++;
    } else {
      str += value[i - 1];
      if (counter !== 1) {
        str += counter;
      }
      counter = 1;
    }
  }
  str += value[value.length - 1];
  if (counter !== 1) {
    str += counter;
  }
  return str;
};
console.log(rle('AVVVBBBVVXDHJFFFFDDDDDDHAAAAJJJDDSLSSSDDDD'));
