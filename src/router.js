import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'main',
      component: () => import(/* webpackChunkName: "about" */ './views/MainPage.vue'),
    },
    {
      path: '/:slug',
      name: 'article-page',
      props: true,
      component: () => import(/* webpackChunkName: "about" */ './views/ArticlePage.vue'),
    },
    {
      path: '/delete',
      name: 'delete-page',
      props: true,
      component: () => import(/* webpackChunkName: "about" */ './views/DeletePage.vue'),
    },
    {
      path: '/edit/:slug',
      name: 'edit-page',
      props: true,
      component: () => import(/* webpackChunkName: "about" */ './views/EditPage.vue'),
    },
    {
      path: '/create',
      name: 'create-page',
      component: () => import(/* webpackChunkName: "about" */ './views/CreatePage.vue'),
    },
  ],
});
