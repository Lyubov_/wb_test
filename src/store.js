import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

function toMap() {
  const meta = new Map();
  meta.set('newest', 'new');
  meta.set('info', 'info');
  return meta;
}

export default new Vuex.Store({
  state: {
    counter: 2,
    articles: [{
      id: 1,
      active_date_from: '2019-09-06',
      active_date_to: '2019-09-16',
      name: 'Статья 1',
      slug: 'first',
      meta_tags: toMap(),
      content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, architecto atque dolorum eius exercitationem impedit magnam maxime modi mollitia nemo pariatur quasi, quis sapiente sint tempora totam unde vitae voluptates.',
    }, {
      id: 2,
      active_date_from: '2019-09-06',
      active_date_to: '2019-09-16',
      name: 'Статья 2',
      slug: 'second',
      meta_tags: toMap(),
      content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, architecto atque dolorum eius exercitationem impedit magnam maxime modi mollitia nemo pariatur quasi, quis sapiente sint tempora totam unde vitae voluptates.',
    }],
  },
  getters: {
    getArticles: state => state.articles,
    getArticle: state => slug => state.articles.find(item => item.slug === slug),
  },
  mutations: {
    DELETE_FROM_STATE(state, slug) {
      state.articles = state.articles.filter(article => article.slug !== slug);
    },

    UPDATE_ARTICLE(state, { id, form }) {
      const index = state.articles.findIndex(article => article.id === id);
      state.articles[index].name = form.name;
      state.articles[index].slug = form.slug;
      state.articles[index].content = form.content;
      state.articles[index].active_date_from = form.dateStart;
      state.articles[index].active_date_to = form.dateEnd;
    },

    CREATE_ARTICLE(state, form) {
      state.articles.push({
        id: ++state.counter,
        name: form.name,
        content: form.content,
        slug: form.slug,
        active_date_from: form.dateStart,
        active_date_to: form.dateEnd,
      });
    },
  },
  actions: {

  },
});
